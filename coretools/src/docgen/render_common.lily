import introspect
import (ContainerGroup,
        container_table,
        group_table) "docgen/common"
import (ClassSym,
        ContainerSym,
        EnumSym,
        ModuleSym,
        ParameterSym) "shared/symbols"

define render_generics(g: List[introspect.TypeEntry]): String
{
    var generics = g.map(|m| m.class_name() )
                    .join(", ")

    if generics: {
        generics = "[" ++ generics ++ "]"
    }

    return generics
}

define render_local_link(sym: ContainerSym, group: ContainerGroup): String
{
    var name = sym.name
    var kind = name

    match sym: {
        case ClassSym(cs):
            if group == ContainerGroup.Exception: {
                kind = "error"
            else:
                kind = "class"
            }
        case EnumSym(es):
            kind = "enum"
        else:
    }

    var result = "<a href='{0}.{1}.html'>{1}</a>".format(kind, name)

    return result
}

define render_parameter_type(m: ModuleSym, type: introspect.TypeEntry): String
{
    var id = type.class_id()
    var container_sym = container_table[id]
    var name = type.class_name()
    var group = group_table[id]

    if container_sym.module_id == m.id: {
        name = render_local_link(container_sym, group)
    }

    var result = name

    match group: {
        case ContainerGroup.Exception:
            # These should be mono because they can't be raised if poly.

        case ContainerGroup.Monomorphic:

        case ContainerGroup.Polymorphic:
            var args = type.inner_types()
            var arg_strings: List[String] = []

            for i in 0...args.size() - 1: {
                var s = render_parameter_type(m, args[i])

                arg_strings.push(s)
            }

            result = name ++ "[" ++ arg_strings.join(", ") ++ "]"

        case ContainerGroup.Function:
            var args = type.inner_types()
            var arg_strings: List[String] = []

            for i in 1...args.size() - 1: {
                var s = render_parameter_type(m, args[i])

                arg_strings.push(s)
            }

            if type.is_vararg_function(): {
                arg_strings[-1] = arg_strings[-1] ++ "..."
            }

            var result_type = args[0]
            var result_group = group_table[result_type.class_id()]
            var result_str = ""

            if result_group != ContainerGroup.Unit: {
                result_str = " => " ++ render_parameter_type(m, result_type)
            }

            var arg_str = arg_strings.join(", ")

            result = "Function(" ++ arg_str ++ result_str ++ ")"

        case ContainerGroup.Optarg:
            var arg = type.inner_types()[0]

            result = "*" ++ render_parameter_type(m, arg)

        case ContainerGroup.Unit:
            # This one's always mono.
    }

    return result
}

define render_result_type(m: ModuleSym, type: introspect.TypeEntry): String
{
    var group = group_table[type.class_id()]
    var result = ""

    if group != ContainerGroup.Unit: {
        result = ": " ++ render_parameter_type(m, type)
    }

    return result
}

define render_parameters(m: ModuleSym,
                         parameters: List[ParameterSym],
                         :is_static static: Boolean): String
{
    var arg_strings: List[String] = []
    var start = (!static).to_i()

    if start >= parameters.size(): {
        return ""
    }

    for i in start...parameters.size() - 1: {
        var p = parameters[i]
        var type = render_parameter_type(m, p.type)
        var arg = p.name ++ ": " ++ type

        arg_strings.push(arg)
    }

    return "(" ++ arg_strings.join(", ") ++ ")"
}
