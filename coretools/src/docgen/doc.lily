import (BaseSym) "shared/symbols"

class DocError(public var @reason: String,
               public var @error_line: String)
{}

class DocErrorSink
{
    public var @errors: List[DocError] = []

    public define add(reason: String, error_line: String)
    {
        var e = DocError(reason, error_line)

        @errors.push(e)
    }
}

var error_sink = DocErrorSink()

define report_doc_errors
{
    var errors = error_sink.errors

    print("Errors encountered while building docs:\n")

    for i in 0...errors.size() - 1: {
        var e = errors[i]
        var template = "    Reason: {0}\n    Source:\n{1}\n"
        var output = template.format(e.reason, e.error_line)

        print(output)
    }

    error_sink.errors = []
}

define have_doc_errors: Boolean
{
    return error_sink.errors.size() != 0
}

define render_one_doc_line(input: String): String
{
    var out_cache: List[String] = []
    var pending_out = ""
    var raw_line = input.to_bytestring()
    var in_backtick = false
    var should_slice = false
    var pos = 0
    var slice_start = 0
    var to_push = ""

    for pos in 0...raw_line.size() - 1: {
        var ch = raw_line[pos]
        if ch == '`': {
            if in_backtick == false: {
                should_slice = true
                to_push = "<code>"
                in_backtick = true
            else:
                should_slice = true
                to_push = "</code>"
                in_backtick = false
            }
        elif ch == '&':
            should_slice = true
            to_push = "&amp;"
        elif ch == '<':
            should_slice = true
            to_push = "&lt;"
        elif ch == '>':
            should_slice = true
            to_push = "&gt;"
        elif ch == '"':
            should_slice = true
            to_push = "&quot;"
        elif ch == '\'':
            should_slice = true
            to_push = "&#39;"
        }

        if should_slice: {
            out_cache.push(input.slice(slice_start, pos))
            out_cache.push(to_push)
            should_slice = false
            slice_start = pos + 1
        }
    }

    if in_backtick: {
        error_sink.add("Unterminated backtick at end.", input)
        out_cache = []
    else:
        input.slice(slice_start) |> out_cache.push
    }

    return out_cache.join()
}

define html_escape(input_line: String): String
{
    var out_cache: List[String] = []
    var pending_out = ""
    var raw_line = input_line.to_bytestring()
    var should_slice = false
    var pos = 0
    var slice_start = 0
    var to_push = ""

    for pos in 0...raw_line.size() - 1: {
        var ch = raw_line[pos]
        if ch == '&': {
            should_slice = true
            to_push = "&amp;"
        elif ch == '<':
            should_slice = true
            to_push = "&lt;"
        elif ch == '>':
            should_slice = true
            to_push = "&gt;"
        elif ch == '"':
            should_slice = true
            to_push = "&quot;"
        elif ch == '\'':
            should_slice = true
            to_push = "&#39;"
        }

        if should_slice: {
            out_cache.push(input_line.slice(slice_start, pos))
            out_cache.push(to_push)
            should_slice = false
            slice_start = pos + 1
        }
    }

    input_line.slice(slice_start) |> out_cache.push
    return out_cache.join()
}

#[
This implements a very small set of markdown, plus a couple of special markups.

It works by splitting along totally blank lines and then assuming that what's
united by only a single newline is part of a whole block.

It has basic support for a bulleted list, replaces backticks with code tags,
replaces html characters, and has some special section markers.

Even though it's a small subset, it's enough for the moment. Eventually, it
should be replaced with a real markdown processor (as long as the special
sections can be kept).
]#
define render_doc(sym: BaseSym, name: String): String
{
    var input_lines = sym.doc.trim().split("\n\n")
    var result_list: List[String] = []

    for i in 0...input_lines.size() - 1: {
        var line = input_lines[i]
        var out_line = ""

        if line.starts_with("```") &&
           line.ends_with("```"): {
            # Assume that fenced code blocks have ``` on lines by themselves.
            # Cut by 4 instead of 3 to omit the leading+trailing newlines.

            line = line.slice(4, -4)
            out_line = "<pre>" ++
                        html_escape(line) ++
                       "</pre>"
            result_list.push(out_line)

            continue
        }

        out_line = render_one_doc_line(line)

        if out_line.starts_with("*"): {
            var list_lines = out_line.split("\n")

            out_line = out_line.split("\n")
                        .select(|s| s.starts_with("*") )
                        .map(|m| "<li>" ++ m.slice(1).lstrip(" ") ++ "</li>\n" )
                        .join()

            out_line = "<ul>\n" ++ out_line ++ "</ul>\n"
        elif out_line.starts_with("#"):
            if out_line == "# Errors": {
                # Assume there's only one of these, and that it will be followed
                # by a bulleted list of errors and what raises them.
                out_line = ("<div class='linkblock' id='errors.{0}'>" ++
                            "<a href='#errors.{0}'>Errors</a>" ++
                            "</div>\n")
                           .format(name)
            elif out_line == "# Examples":
                # This assumes only one 'example' section per doc block.
                # This should be followed by a fenced code block.
                # Having multiple single-line examples in the block is
                # encouraged.
                out_line = ("<div class='linkblock' id='examples.{0}'>" ++
                            "<a href='#examples.{0}'>Examples</a>" ++
                            "</div>\n")
                           .format(name)
            else:
                out_line = "<p>" ++ out_line ++ "</p>\n"
            }
        else:
            out_line = "<p>" ++ out_line ++ "</p>\n"
        }

        result_list.push(out_line)
    }

    var result = result_list.join()

    return result
}

define render_shortdesc(sym: BaseSym): String
{
    var doc = sym.doc.trim()

    match doc.find("\n\n"): {
        case Some(s):
            doc = doc.slice(0, s)
        case None:
    }

    var result = render_one_doc_line(doc)

    return result
}
